// os345.c - OS Kernel	09/12/2013
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the BYU CS345 projects.      **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <time.h>
#include <assert.h>
#include <math.h>

#include "os345config.h"
#include "os345signals.h"
#include "stddef.h"
#include "os345.h"
#include "os345lc3.h"
#include "os345fat.h"
#include "pqueue.h"

// **********************************************************************
//	local prototypes
//
void pollInterrupts(void);
static int scheduler(void);
static int dispatcher(void);
void assignTaskTimes();

//static void keyboard_isr(void);
//static void timer_isr(void);

int sysKillTask(int taskId);
static int initOS(void);

// **********************************************************************
// **********************************************************************
// global semaphores

Semaphore* semaphoreList;			// linked list of active semaphores

Semaphore* keyboard;				// keyboard semaphore
Semaphore* charReady;				// character has been entered
Semaphore* inBufferReady;			// input buffer ready semaphore

Semaphore* tics10sec;				// 1 second semaphore
Semaphore* tics1sec;				// 1 second semaphore
Semaphore* tics10thsec;				// 1/10 second semaphore

Semaphore* dcChange;				// 1 if deltaclock has changed
Semaphore* dcAccess;				// 1 if deltaclock can be accessed

// **********************************************************************
// **********************************************************************
// global system variables

TCB tcb[MAX_TASKS];					// task control block
Semaphore* taskSems[MAX_TASKS];		// task semaphore
jmp_buf k_context;					// context of kernel stack
jmp_buf reset_context;				// context of kernel stack
volatile void* temp;				// temp pointer used in dispatcher

int scheduler_mode;					// scheduler mode
int superMode;						// system mode
int curTask;						// current task #
long swapCount;						// number of re-schedule cycles
char inChar;						// last entered character
int charFlag;						// 0 => buffered input
int inBufIndx;						// input pointer into input buffer
char inBuffer[INBUF_SIZE+1];		// character input buffer
//Message messages[NUM_MESSAGES];		// process message buffers

int pollClock;						// current clock()
int lastPollClock;					// last pollClock
bool diskMounted;					// disk has been mounted

time_t oldTime1;					// old 1sec time
time_t oldTime10;					// old 1sec time
clock_t myClkTime;
clock_t myOldClkTime;

int* rq;							// ready priority queue tasks
int* rqp;							// ready priority queue priorities

DCnode* deltaclock;


// **********************************************************************
// **********************************************************************
// OS startup
//
// 1. Init OS
// 2. Define reset longjmp vector
// 3. Define global system semaphores
// 4. Create CLI task
// 5. Enter scheduling/idle loop
//
int main(int argc, char* argv[])
{
	// save context for restart (a system reset would return here...)
	int resetCode = setjmp(reset_context);
	superMode = TRUE;						// supervisor mode

	switch (resetCode)
	{
		case POWER_DOWN_QUIT:				// quit
			powerDown(0);
			printf("\nGoodbye!!");
			return 0;

		case POWER_DOWN_RESTART:			// restart
			powerDown(resetCode);
			printf("\nRestarting system...\n");

		case POWER_UP:						// startup
			break;

		default:
			printf("\nShutting down due to error %d", resetCode);
			powerDown(resetCode);
			return resetCode;
	}

	// output header message
	printf("%s", STARTUP_MSG);

	// initalize OS
	if ( resetCode = initOS()) return resetCode;

	// create global/system semaphores here
	//?? vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

	charReady = createSemaphore("charReady", BINARY, 0);
	inBufferReady = createSemaphore("inBufferReady", BINARY, 0);
	keyboard = createSemaphore("keyboard", BINARY, 1);
	tics10sec = createSemaphore("tics10sec", COUNTING, 0);
	tics1sec = createSemaphore("tics1sec", BINARY, 0);
	tics10thsec = createSemaphore("tics10thsec", BINARY, 0);
	dcChange = createSemaphore("deltaclockchanged", BINARY, 0);
	dcAccess = createSemaphore("deltaclockaccess", BINARY, 1);

	//?? ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	// schedule CLI task
	createTask("myShell",			// task name
					P1_shellTask,	// task
					MED_PRIORITY,	// task priority
					argc,			// task arg count
					argv);			// task argument pointers

	// HERE WE GO................

	// Scheduling loop
	// 1. Check for asynchronous events (character inputs, timers, etc.)
	// 2. Choose a ready task to schedule
	// 3. Dispatch task
	// 4. Loop (forever!)

	while(1)									// scheduling loop
	{
		// check for character / timer interrupts
		pollInterrupts();

		// schedule highest priority ready task
		if ((curTask = scheduler()) < 0) continue;

		// dispatch curTask, quit OS if negative return
		if (dispatcher() < 0) break;
	}											// end of scheduling loop

	// exit os
	longjmp(reset_context, POWER_DOWN_QUIT);
	return 0;
} // end main



// **********************************************************************
// **********************************************************************
// scheduler
//
int firstTaskNotExecuted= -1;
int allZeros = 1;
long int group_count[NUM_PARENTS];   // parent group counters
static int scheduler(void)
{
	int nextTask;

	nextTask = deQ(rq, rqp, -1);
	if (nextTask != -1)
	{
		enQ(rq, rqp, nextTask, tcb[nextTask].priority);
		if (nextTask == 0 || strstr(tcb[nextTask].name, "Group Report") != NULL)
		{
			return nextTask;
		}
		if (scheduler_mode == 1) // fair
		{
			/*
			printf("\n%s time %d", tcb[nextTask].name, tcb[nextTask].time);
			int sum = 0;
			for (int i = 0; i < NUM_PARENTS; ++i) sum += group_count[i];

			printf("\nGroups:");
			for (int i=0; sum>0 && i<NUM_PARENTS; i++)
				printf("%10ld (%d%%)", group_count[i], (group_count[i] * 100) / sum);
				*/

			if (tcb[nextTask].time == 0) // cannot be executed
			{
				//printf("\nis 0");
				if (firstTaskNotExecuted == nextTask) // sam: we're going in circles
				{
					if (allZeros)
						assignTaskTimes();
					allZeros = 1;
					//printf("\n\nscheduling");
				}
				if (firstTaskNotExecuted == -1)
					firstTaskNotExecuted = nextTask;
				return -1;
			}
			else // can be executed
			{
				//printf("\nis NOT 0");
				tcb[nextTask].time--;
				allZeros = 0;
				firstTaskNotExecuted = -1;
				//printf("\nreturn TID %d", nextTask);
				return nextTask;
			}
		}
	}
	if (tcb[nextTask].signal & mySIGSTOP)
		return -1;

	return nextTask;
} // end scheduler


void assignTaskTimes()
{
	// find the parents
	//printf("\nfinding parents");
	int parents[NUM_PARENTS];
	int numParents = 0;
	for (int i=0; i<MAX_TASKS; i++)
	{
		if (!tcb[i].name)
			continue;
		//printf("\nread: %d %s", i, tcb[i].name);
		if (strstr(tcb[i].name, "parent") != NULL)
		{
			parents[numParents++] = i;
			//printf("\nfound parent %s", tcb[i].name);
		}
	}
	//printf("\nnumParents: %d", numParents);

	// count children
	//printf("\ncounting children");
	int largestNumChildren = 0;
	for (int i=0; i<numParents; i++)
	{
		int numChildren = 0;
		for (int j=0; j<MAX_TASKS; j++)
		{
			if (tcb[j].parent == parents[i])
				numChildren++;
		}
		if (numChildren > largestNumChildren)
			largestNumChildren = numChildren;
	}
	//printf("\nlargestNumChildren: %d", largestNumChildren);

	// find total time resource needed
	//printf("\nfinding total");
	int timePerGroup = largestNumChildren+1; // don't forget the parent (+1)
	//printf("\ntimePerGroup: %d", timePerGroup);

	// assign the time resource
	//printf("\nassigning time");
	int totalTimeGiven = 0;
	for (int i=0; i<numParents; i++) // for each parent
	{
		// count children
		int numChildren = 0;
		for (int j=0; j<MAX_TASKS; j++) // for each child
		{
			if (tcb[j].parent != parents[i])
				continue;
			numChildren++;
		}

		// assign time
		int timePerProcess = timePerGroup / (numChildren+1);
		//printf("\ntimePerProcess: %d", timePerProcess);
		int timePerProcessRemainder = timePerGroup % (numChildren+1);
		//printf("\ntimePerProcessRemainder: %d", timePerProcessRemainder);
		tcb[parents[i]].time = timePerProcess + timePerProcessRemainder;
		totalTimeGiven += tcb[parents[i]].time;
		//printf("\n%s assigned %d time", tcb[parents[i]].name, tcb[parents[i]].time);
		for (int j=0; j<MAX_TASKS; j++) // for each child
		{
			if (tcb[j].parent != parents[i])
				continue;
			tcb[j].time = timePerProcess;
			totalTimeGiven += tcb[j].time;
			//printf("\n%s assigned %d time", tcb[j].name, timePerProcess);
		}
	}

	// make sure the proper amount of time was given
	assert(totalTimeGiven == timePerGroup * numParents);
	int prevGroupTime = -1;
	for (int i=0; i<numParents; i++)
	{
		int groupTime = tcb[parents[i]].time;
		for (int j=0; j<MAX_TASKS; j++)
		{
			if (tcb[j].parent == parents[i])
				groupTime += tcb[j].time;
		}
		if (i > 0)
		{
			if (groupTime != prevGroupTime)
			{
				printf("\ngroupTime: %d prevGroupTime: %d", groupTime, prevGroupTime);
				assert(0);
			}
		}
		prevGroupTime = groupTime;
	}

	//printf("\n");
}

// **********************************************************************
// **********************************************************************
// dispatch curTask
//
static int dispatcher()
{
	int result;

	// schedule task
	switch(tcb[curTask].state)
	{
		case S_NEW:
		{
			// new task
			printf("\nNew Task[%d] %s", curTask, tcb[curTask].name);
			tcb[curTask].state = S_RUNNING;	// set task to run state

			// save kernel context for task SWAP's
			if (setjmp(k_context))
			{
				superMode = TRUE;					// supervisor mode
				break;								// context switch to next task
			}

			// move to new task stack (leave room for return value/address)
			temp = (int*)tcb[curTask].stack + (STACK_SIZE-8);
			SET_STACK(temp);
			superMode = FALSE;						// user mode

			// begin execution of new task, pass argc, argv
			result = (*tcb[curTask].task)(tcb[curTask].argc, tcb[curTask].argv);

			// task has completed
			if (result) printf("\nTask[%d] returned %d", curTask, result);
			else printf("\nTask[%d] returned %d", curTask, result);
			tcb[curTask].state = S_EXIT;			// set task to exit state

			// return to kernal mode
			longjmp(k_context, 1);					// return to kernel
		}

		case S_READY:
		{
			tcb[curTask].state = S_RUNNING;			// set task to run
		}

		case S_RUNNING:
		{
			if (setjmp(k_context))
			{
				// SWAP executed in task
				superMode = TRUE;					// supervisor mode
				break;								// return from task
			}
			if (signals()) break;
			longjmp(tcb[curTask].context, 3); 		// restore task context
		}

		case S_BLOCKED:
		{
			break;
		}

		case S_EXIT:
		{
			if (curTask == 0) return -1;			// if CLI, then quit scheduler
			// release resources and kill task
			//printf("\nkilling task %d", curTask);
			sysKillTask(curTask);					// kill current task
			break;
		}

		default:
		{
			printf("Unknown Task[%d] State", curTask);
			longjmp(reset_context, POWER_DOWN_ERROR);
		}
	}
	return 0;
} // end dispatcher



// **********************************************************************
// **********************************************************************
// Do a context switch to next task.

// 1. If scheduling task, return (setjmp returns non-zero value)
// 2. Else, save current task context (setjmp returns zero value)
// 3. Set current task state to READY
// 4. Enter kernel mode (longjmp to k_context)

void swapTask()
{
	assert("SWAP Error" && !superMode);		// assert user mode

	// increment swap cycle counter
	swapCount++;

	// either save current task context or schedule task (return)
	if (setjmp(tcb[curTask].context))
	{
		superMode = FALSE;					// user mode
		return;
	}

	// context switch - move task state to ready
	if (tcb[curTask].state == S_RUNNING) tcb[curTask].state = S_READY;

	// move to kernel mode (reschedule)
	longjmp(k_context, 2);
} // end swapTask



// **********************************************************************
// **********************************************************************
// system utility functions
// **********************************************************************
// **********************************************************************

// **********************************************************************
// **********************************************************************
// initialize operating system
static int initOS()
{
	int i;

	// make any system adjustments (for unblocking keyboard inputs)
	INIT_OS

	// reset system variables
	curTask = 0;						// current task #
	swapCount = 0;						// number of scheduler cycles
	scheduler_mode = 0;					// scheduler (0 = prioritized round-robin) (1 = fair scheduler)
	inChar = 0;							// last entered character
	charFlag = 0;						// 0 => buffered input
	inBufIndx = 0;						// input pointer into input buffer
	semaphoreList = 0;					// linked list of active semaphores
	diskMounted = 0;					// disk has been mounted

	// init ready queue
	rq = (int*)malloc(MAX_TASKS * sizeof(int));
	rqp = (int*)malloc(MAX_TASKS * sizeof(int));
	rq[0] = 0; // the first element is the size
	rqp[0] = 0; // the first element is the size
	if (rq == NULL) return 99;
	//testQ(rq, rqp);

	// init delta clock
	//deltaclock = createDCnode(); //TODO: WHY DOESN'T THIS WORK!?!?!?!?
	deltaclock = (DCnode*)malloc(sizeof(DCnode));
	if (deltaclock == NULL)
		assert(FALSE);
	deltaclock->time = 0;
	deltaclock->next = NULL;
	deltaclock->sem = NULL;
	/*
	printDeltaClock(deltaclock);
	insertDeltaClock(deltaclock, 5, NULL);
	printDeltaClock(deltaclock);
	insertDeltaClock(deltaclock, 7, NULL);
	printDeltaClock(deltaclock);
	insertDeltaClock(deltaclock, 1, NULL);
	printDeltaClock(deltaclock);
	*/
	//printf("\ndc: %d", deltaclock);
	//printf("\nnext: %d", deltaclock->next);
	//insertDeltaClock(deltaclock, 4, 0);
	//printf("\ntime: %d", deltaclock->time);

	// capture current time
	lastPollClock = clock();			// last pollClock
	time(&oldTime1);
	time(&oldTime10);

	// init system tcb's
	for (i=0; i<MAX_TASKS; i++)
	{
		tcb[i].name = NULL;				// tcb
		taskSems[i] = NULL;				// task semaphore
	}

	// init tcb
	for (i=0; i<MAX_TASKS; i++)
	{
		tcb[i].name = NULL;
	}

	// initialize lc-3 memory
	initLC3Memory(LC3_MEM_FRAME, 0xF800>>6);

	// ?? initialize all execution queues

	return 0;
} // end initOS



// **********************************************************************
// **********************************************************************
// Causes the system to shut down. Use this for critical errors
void powerDown(int code)
{
	int i;
	printf("\nPowerDown Code %d", code);

	// release all system resources.
	printf("\nRecovering Task Resources...");

	// kill all tasks
	for (i = MAX_TASKS-1; i >= 0; i--)
		if(tcb[i].name) sysKillTask(i);

	// delete all semaphores
	while (semaphoreList)
		deleteSemaphore(&semaphoreList);

	// free ready queue
	free(rq);

	// ?? release any other system resources
	// ?? deltaclock (project 3)
	DCnode* n = deltaclock;
	while (n != NULL)
	{
		DCnode* prev = n;
		n = n->next;
		//printf("\nfreed a node! %d", prev->time);
		free(prev);
	}

	RESTORE_OS
	return;
} // end powerDown


