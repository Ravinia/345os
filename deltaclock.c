#include "deltaclock.h"
#include "stddef.h"
#include <stdlib.h>

extern Semaphore* dcChange;				   // 1 if deltaclock has been changed
extern Semaphore* dcAccess;				   // 1 if deltaclock can be accessed
extern Semaphore* semaphoreList;           // linked list of active semaphores

extern Semaphore* keyboard;                // keyboard semaphore
extern Semaphore* charReady;               // character has been entered
extern Semaphore* inBufferReady;           // input buffer ready semaphore

extern Semaphore* tics10sec;               // 1 second semaphore
extern Semaphore* tics1sec;                // 1 second semaphore
extern Semaphore* tics10thsec;             // 1/10 second semaphore


void insertDeltaClock(DCnode* root, int time, Semaphore* sem)
{
	SEM_WAIT(dcAccess);SWAP;
	root->time++;SWAP;

	// setup
	DCnode* prev = root;SWAP;
	DCnode* n = root->next;SWAP;

	// iterate until time goes negative
	while (n != NULL)
	{
		time -= n->time;SWAP;
		if (time < 0)
			break;SWAP;
		prev = n;SWAP;
		n = n->next;SWAP;
	}

	// create the new node
	DCnode* insert = 0;SWAP;
	if (n == NULL)
	{
		prev->next = createDCnode();SWAP;
		n = prev->next;SWAP;
	}
	else
		insert = createDCnode();SWAP;

	// init and insert the new node
	if (time <= 0 && insert != 0)
	{
		insert->time = -time;SWAP;
		insert->sem = n->sem;SWAP;
		insert->next = n->next;SWAP;
		n->next = insert;SWAP;
	}

	// init the new node or the insert-1 node
	n->time += time;SWAP;
	n->sem = sem;SWAP;
	SEM_SIGNAL(dcAccess);SWAP;
	SEM_SIGNAL(dcChange);SWAP;
}

void printDeltaClock(DCnode* root)
{
	printf("\nDelta Clock");SWAP;
	SEM_WAIT(dcAccess);SWAP;
	DCnode* p = root->next;SWAP;
	for (int i=1; i<root->time+1; i++)
	{
		printf("\n\t%d ", p->time);SWAP;
		if (p->sem != NULL)
			printf(" %s", p->sem->name);SWAP;
		p = p->next;SWAP;
	}
	SEM_SIGNAL(dcAccess);SWAP;
}

DCnode* createDCnode()
{
	DCnode* dcnode = malloc(sizeof(DCnode));SWAP;
	if (dcnode == NULL)
		return NULL;SWAP;
	dcnode->time = 0;SWAP;
	dcnode->next = NULL;SWAP;
	dcnode->sem = NULL;SWAP;
	//printf("\ncreated node: time: %d", dcnode->time);SWAP;
	SEM_SIGNAL(dcChange);SWAP;
	return dcnode;
}

void testDeltaClock()
{
	DCnode* dc = createDCnode();SWAP;
	insertDeltaClock(dc, 4, semaphoreList);SWAP;
	printDeltaClock(dc);SWAP;
	insertDeltaClock(dc, 7, keyboard);SWAP;
	printDeltaClock(dc);SWAP;
	insertDeltaClock(dc, 7, charReady);SWAP;
	printDeltaClock(dc);SWAP;
	insertDeltaClock(dc, 7, inBufferReady);SWAP;
	printDeltaClock(dc);SWAP;
	insertDeltaClock(dc, 50, tics10sec);SWAP;
	printDeltaClock(dc);SWAP;
	insertDeltaClock(dc, 1, tics1sec);SWAP;
	printDeltaClock(dc);SWAP;
	insertDeltaClock(dc, 1, tics10thsec);SWAP;
	printDeltaClock(dc);SWAP;
	insertDeltaClock(dc, 40, dcAccess);SWAP;
	printDeltaClock(dc);SWAP;
	free(dc);SWAP;
}
