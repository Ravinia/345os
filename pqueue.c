#include "pqueue.h"

// returns the tid parameter
int enQ(int* q, int* qp, TID tid, int p)
{
	if (q[0] == MAX_TASKS-1)
		return -1;

	q[0]++;
	qp[0]++;

	int i = -1;
	for(i=q[0]; i>0; i--)
	{
		if (qp[i-1] < p && i > 1)
		{
			q[i] = q[i-1];
			qp[i] = qp[i-1];
		}
		else
			break;
	}
	q[i] = tid;
	qp[i] = p;

	return tid;
}

// returns the tid of the task that was dequeued or -1 if nothing was
int deQ(int* q, int* qp, TID tid)
{
	if (tid == -1)
		tid = q[1];

	int found = 0;
	for (int i=1; i<q[0]+1; i++)
	{
		if (q[i] == tid && !found)
		{
			found = 1;
			q[0]--;
			qp[0]--;
		}
		if (found)
		{
			q[i] = q[i+1];
			qp[i] = qp[i+1];
		}
	}

	if (found)
		return tid;
	return -1;
}

void printQ(int* q, int* qp)
{   
	printf("\nqueue: ");
	for (int i=0; i<q[0]+1; i++)
		printf("%d ", q[i]);

	//printf("\nprior: ");
	//for (int i=0; i<q[0]+1; i++)
		//printf("%d ", qp[i]);
}

int Qcontains(int* q, int* qp, int value)
{
	for (int i=1; i<q[0]+1; i++)
	{
		if (q[i] == value)
			return 1;
	}
	return 0;
}

void testQ(int* q, int* qp)
{   
	/*
	printf("\ntesting pq");

	printQ(q, qp);
	enQ(q, qp, 3, 3);
	printQ(q, qp);
	enQ(q, qp, 2, 2);
	printQ(q, qp);
	enQ(q, qp, 4, 4);
	printQ(q, qp);
	enQ(q, qp, 1, 1);
	printQ(q, qp);
	enQ(q, qp, 1, 1);
	printQ(q, qp);
	enQ(q, qp, 1, 1);
	printQ(q, qp);
	enQ(q, qp, 9, 9);
	printQ(q, qp);
	enQ(q, qp, 7, 7);
	printQ(q, qp);
	enQ(q, qp, 7, 7);
	printQ(q, qp);

	int ret = deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	deQ(q, qp, -1);
	printQ(q, qp);
	*/
}
