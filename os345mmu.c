// os345mmu.c - LC-3 Memory Management Unit	03/12/2015
//
//		03/12/2015	added PAGE_GET_SIZE to accessPage()
//
// **************************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <assert.h>
#include "os345.h"
#include "os345lc3.h"

// ***********************************************************************
// mmu variables

// LC-3 memory
unsigned short int memory[LC3_MAX_MEMORY];

// statistics
int memAccess;						// memory accesses
int memHits;						// memory hits
int memPageFaults;					// memory faults

int getFrame(int);
int runClock();
int getAvailableFrame(void);
extern TCB tcb[];					// task control block
extern int curTask;					// current task #

int getFrame(int notme)
{
	int frame;
	frame = getAvailableFrame();
	//printf("\ngot frame: %d", frame);
	if (frame >=0) return frame;

	// run clock
	//printf("\nWe're toast!!!!!!!!!!!!");
	frame = runClock(notme);

	return frame;
}

int bigHand = LC3_RPT; // use to loop over rpte's
int smallHand = -1; // used to loop over upte's
void performSwap(int type);
int userFrameIsEmpty();

#define SWAP_USER_FRAMES 1

int runClock(int notme)
{
	while (1) // for each rpte
	{
		int rpte1 = memory[bigHand];
		int rpte2 = memory[bigHand+1];

		//printf("\nrpte1: %x", rpte1);
		//printf("\nrpte2: %x", rpte2);

		if (DEFINED(rpte1))
		{
			//printf("\nsearching rpta: %x", bigHand);
			if (REFERENCED(rpte1) && smallHand == -1) // only outer loop if previous inner loop is done
			{
				//printf("\ncleared ref rpte");
				rpte1 = memory[bigHand] = CLEAR_REF(rpte1);
			}
			else
			{
				int upta = (FRAME(rpte1)<<6);	// user page table address
				int uptaEnd = upta + LC3_FRAME_SIZE;
				//printf("\nuptaEnd: %x", uptaEnd);

				if (smallHand == -1) // only start new loop if finished last smallHand loop
				{
					smallHand = upta;
					//printf("\nreinit small hand");
				}

				for (; smallHand < uptaEnd; smallHand += 2) // for each upte (each entry is 2 words)
				{
					int upte1 = memory[smallHand];
					int upte2 = memory[smallHand+1];
					if (DEFINED(upte1))
					{
						//printf("\nsmallHand: %x", smallHand);
						if (REFERENCED(upte1))
						{
							//printf("\ncleared ref upte");
							upte1 = memory[smallHand] = CLEAR_REF(upte1);
						}
						else // time to swap data frame
						{
							//printf("\nswapping data page: %x", smallHand);
							performSwap(0); // order
							smallHand += 2; // matters here
							return FRAME(upte1);
						}
					}
				}

				#if SWAP_USER_FRAMES
				smallHand = -1; // didn't find a frame in the inner loop
				if (!REFERENCED(rpte1) && userFrameIsEmpty() && FRAME(rpte1) != notme) // time to swap user frame
				{
					//printf("\nswapping user page: %x", bigHand);
					performSwap(1); // order
					bigHand += 2; // matters here
					return FRAME(rpte1);
				}
				#endif
			}
		}
		bigHand += 2; // each entry is 2 words
		if (bigHand >= LC3_RPT_END)
			bigHand = LC3_RPT;
	}
}

int userFrameIsEmpty()
{
	int rpte1 = memory[bigHand];
	int upta = FRAME(rpte1)<<6;
	int uptaEnd = upta+LC3_FRAME_SIZE;

	for (int i=upta; i<uptaEnd; i+=2) // for each upte (each entry is 2 words)
	{
		int upte1 = memory[i];
		if (DEFINED(upte1))
			return 0;
	}

	return 1;
}

// type 0 means data frame
// type 1 means user frame
void performSwap(int type)
{
	int isPaged;
	int page;
	int frame;

	if (type == 1) // swap user frame
	{
		#if !SWAP_USER_FRAMES
		assert(0); // you ain't allowed to do that
		#endif
		frame = FRAME(memory[bigHand]);
		//printf("\nswapping user frame: %d", frame);
		isPaged = PAGED(memory[bigHand+1]);
		if (isPaged)
		{
			page = SWAPPAGE(memory[bigHand+1]);
			//printf("\npage: %d", page);
			if (DIRTY(memory[bigHand])) // save if dirty
				accessPage(page, frame, PAGE_OLD_WRITE);
		}
		memory[bigHand] = 0; //clear defined!
	}
	else // swap data frame
	{
		frame = FRAME(memory[smallHand]);
		//printf("\nswapping data frame: %d", frame);
		isPaged = PAGED(memory[smallHand+1]);
		if (isPaged)
		{
			page = SWAPPAGE(memory[smallHand+1]);
			//printf("\npage: %d", page);
			if (DIRTY(memory[smallHand])) // save if dirty
				accessPage(page, frame, PAGE_OLD_WRITE);
		}
		memory[smallHand] = 0; //clear defined!
	}


	if (!isPaged)// make new entry in swap table
	{
		int nextPage = accessPage(-1, -1, PAGE_GET_SIZE);
		if (type == 1)
		{
			memory[bigHand] = 0; // wipe out now inaccurate frame info
			memory[bigHand+1] = SET_PAGED(nextPage);
		}
		else
		{
			memory[smallHand] = 0; // wipe out now inaccurate frame info
			memory[smallHand+1] = SET_PAGED(nextPage);
		}
		accessPage(nextPage, frame, PAGE_NEW_WRITE);
		//printf("\nmade page: %d", nextPage);
	}
}


// **************************************************************************
// **************************************************************************
// LC3 Memory Management Unit
// Virtual Memory Process
// **************************************************************************
//           ___________________________________Frame defined
//          / __________________________________Dirty frame
//         / / _________________________________Referenced frame
//        / / / ________________________________Pinned in memory
//       / / / /     ___________________________
//      / / / /     /                 __________frame # (0-1023) (2^10)
//     / / / /     /                 / _________page defined
//    / / / /     /                 / /       __page # (0-4096) (2^12)
//   / / / /     /                 / /       /
//  / / / /     / 	             / /       /
// F D R P - - f f|f f f f f f f f|S - - - p p p p|p p p p p p p p

#define MMU_ENABLE	1

unsigned short int *getMemAdr(int va, int rwFlg)
{
	//printf("\nrwFlg: %d", rwFlg);
	//printf("\nva: %x", va);
	unsigned short int pa;
	int rpta, rpte1, rpte2;
	int upta, upte1, upte2;
	int rptFrame, uptFrame;

	// each frame is 1024 bits, 128 bytes, 64 words

	// turn off virtual addressing for system RAM
	if (va < 0x3000) return &memory[va];
#if MMU_ENABLE
	//printf("\ntask rpt: %x", tcb[curTask].RPT);
	//printf("\nbase rpt: %x", RPTI(va));

	memAccess++;

	rpta = tcb[curTask].RPT + RPTI(va);		// root page table address
	//printf("\nrpta: %x", rpta);
	//printf("\nrpta num: %d", rpta);
	rpte1 = memory[rpta];
	//printf("\nrpta1 before: %x", rpte1);
	rpte2 = memory[rpta+1];

	if (DEFINED(rpte1))						// rpte defined
	{
		//printf("\ndefined rpte1");
		memHits++;
	}
	else									// rpte undefined
	{
		memPageFaults++;
		//printf("\nrpte1 init: %x", rpte1);
		//printf("\nrpte2 init: %x", rpte2);
		// get a frame
		int frame = getFrame(-1); // rtpe isn't in accessable ram, so no use for notme flag here
		int frameAdr = frame*LC3_FRAME_SIZE;
		//printf("\nrootFrameNum: %d", frame);
		//printf("\nrootframeNumHex: %x", frame);
		//printf("\nrootframeAdr: %d", frameAdr);
		//printf("\nrootframeHex: %x", frameAdr);

		//rpte1 = memory[rpta];
		//rpte2 = memory[rpta+1];
		if (PAGED(rpte2)) // load the frame from swap if it's out there
		{
			accessPage(SWAPPAGE(rpte2), frame, PAGE_READ);
			//printf("\nloading user frame from swap: %d", frame);
		}
		else // initialize entire upt frame
		{
			for (int i=0; i<LC3_FRAME_SIZE; i++)
				memory[frameAdr+i] = 0;
		}
		
		// update
		rpte1 = memory[rpta] = SET_DEFINED(frame);		// set rpt frame defined bit
	}
	//printf("\nrpte1 set: %x", rpte1);

	upta = (FRAME(rpte1)<<6) + UPTI(va);	// user page table address
	//printf("\nupta: %x", upta);
	upte1 = memory[upta];
	upte2 = memory[upta+1];

	if (DEFINED(upte1))						// upte defined
	{
		memHits++;
	}	
	else									// upte undefined
	{
		memPageFaults++;
		//printf("\nupte1 init: %x", upte1);
		int frame = getFrame(FRAME(rpte1));
		int frameAdr = frame*LC3_FRAME_SIZE;
		//printf("\nuserFrameNum: %d", frame);
		//printf("\nuserframeNumHex: %x", frame);
		//printf("\nuserframeAdr: %d", frameAdr);
		//printf("\nuserframeHex: %x", frameAdr);

		//upte1 = memory[upta];
		//upte2 = memory[upta+1];
		if (PAGED(upte2)) // load the frame from swap if it's out there
		{
			//printf("\nloading data frame from swap: %d", frame);
			accessPage(SWAPPAGE(upte2), frame, PAGE_READ);
		}
		else // initialize entire data frame (there's no harm in this, right?)
		{
			for (int i=0; i<LC3_FRAME_SIZE; i++)
				memory[frameAdr+i] = 0;
		}

		//update
		upte1 = memory[upta] = SET_DEFINED(frame);
		rpte1 = memory[rpta] = SET_DIRTY(rpte1); // the upt has changed, so set the corresponding rpt to 
	}

	rpte1 = memory[rpta] = SET_REF(rpte1);
	if (rwFlg)
		rpte1 = memory[rpta] = SET_DIRTY(rpte1);

	upte1 = memory[upta] = SET_REF(upte1);
	if (rwFlg)
		upte1 = memory[upta] = SET_DIRTY(upte1);
	//printf("\nupte1 set: %x", upte1);

	pa = (FRAME(upte1)<<6) + FRAMEOFFSET(va);
	//printf("\nframe utpe1 << 6: %x", FRAME(upte1)<<6);
	//printf("\nframe offset: %x", FRAMEOFFSET(va));
	//printf("\nphysical: %x\n", pa);
	return &memory[pa];
#else
	return &memory[va];
#endif
} // end getMemAdr


// **************************************************************************
// **************************************************************************
// set frames available from sf to ef
//    flg = 0 -> clear all others
//        = 1 -> just add bits
//
void setFrameTableBits(int flg, int sf, int ef)
{	int i, data;
	int adr = LC3_FBT-1;             // index to frame bit table
	int fmask = 0x0001;              // bit mask

	// 1024 frames in LC-3 memory
	for (i=0; i<LC3_FRAMES; i++)
	{	if (fmask & 0x0001)
		{  fmask = 0x8000;
			adr++;
			data = (flg)?MEMWORD(adr):0;
		}
		else fmask = fmask >> 1;
		// allocate frame if in range
		if ( (i >= sf) && (i < ef)) data = data | fmask;
		MEMWORD(adr) = data;
	}
	return;
} // end setFrameTableBits


// **************************************************************************
// get frame from frame bit table (else return -1)
int getAvailableFrame()
{
	int i, data;
	int adr = LC3_FBT - 1;				// index to frame bit table
	int fmask = 0x0001;					// bit mask

	for (i=0; i<LC3_FRAMES; i++)		// look thru all frames
	{	if (fmask & 0x0001)
		{  fmask = 0x8000;				// move to next work
			adr++;
			data = MEMWORD(adr);
		}
		else fmask = fmask >> 1;		// next frame
		// deallocate frame and return frame #
		if (data & fmask)
		{  MEMWORD(adr) = data & ~fmask;
			return i;
		}
	}
	return -1;
} // end getAvailableFrame



// **************************************************************************
// read/write to swap space
int accessPage(int pnum, int frame, int rwnFlg)
{
	static int nextPage;						// swap page size
	static int pageReads;						// page reads
	static int pageWrites;						// page writes
	static unsigned short int swapMemory[LC3_MAX_SWAP_MEMORY];

	if ((nextPage >= LC3_MAX_PAGE) || (pnum >= LC3_MAX_PAGE))
	{
		printf("\nVirtual Memory Space Exceeded!  (%d)", LC3_MAX_PAGE);
		exit(-4);
	}
	switch(rwnFlg)
	{
		case PAGE_INIT:                    		// init paging
			memAccess = 0;						// memory accesses
			memHits = 0;						// memory hits
			memPageFaults = 0;					// memory faults
			nextPage = 0;						// disk swap space size
			pageReads = 0;						// disk page reads
			pageWrites = 0;						// disk page writes
			return 0;

		case PAGE_GET_SIZE:                    	// return swap size
			return nextPage;

		case PAGE_GET_READS:                   	// return swap reads
			return pageReads;

		case PAGE_GET_WRITES:                    // return swap writes
			return pageWrites;

		case PAGE_GET_ADR:                    	// return page address
			//printf("getting address");
			return (int)(&swapMemory[pnum<<6]);

		case PAGE_NEW_WRITE:                   // new write (Drops thru to write old)
			pnum = nextPage++;

		case PAGE_OLD_WRITE:                   // write
			//printf("\n    (%d) Write frame %d (memory[%04x]) to page %d", p.PID, frame, frame<<6, pnum);
			memcpy(&swapMemory[pnum<<6], &memory[frame<<6], 1<<7);
			pageWrites++;
			return pnum;

		case PAGE_READ:                    	// read
			//printf("\n    (%d) Read page %d into frame %d (memory[%04x])", p.PID, pnum, frame, frame<<6);
			memcpy(&memory[frame<<6], &swapMemory[pnum<<6], 1<<7);
			pageReads++;
			return pnum;

		case PAGE_FREE:                   // free page
			printf("\nPAGE_FREE not implemented");
			break;
   }
   return pnum;
} // end accessPage
