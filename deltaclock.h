#ifndef __deltaclock_c__
#define __deltaclock_c__

#include <setjmp.h>
#include "os345.h"

void insertDeltaClock(DCnode* root, int time, Semaphore* sem);
void printDeltaClock(DCnode* root);
DCnode* createDCnode();
void testDeltaClock();

#endif
