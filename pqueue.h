#ifndef __pqueue_c__
#define __pqueue_c__

#include <setjmp.h>
#include "os345.h"

int enQ(int* q, int* qp, TID tid, int p);

int deQ(int* q, int* qp, TID tid);

void printQ(int* q, int* qp);

int Qcontains(int* q, int* qp, int value);

void testQ(int* q, int* qp);

#endif
