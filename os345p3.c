// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <time.h>
#include <assert.h>
#include "os345.h"
#include "os345park.h"

// ***********************************************************************
// project 3 variables

// Jurassic Park
extern JPARK myPark;
extern Semaphore* parkMutex;						// protect park access
extern Semaphore* fillSeat[NUM_CARS];			// (signal) seat ready to fill
extern Semaphore* seatFilled[NUM_CARS];		// (wait) passenger seated
extern Semaphore* rideOver[NUM_CARS];			// (signal) ride over
extern Semaphore* dcChange;
extern Semaphore* dcAccess;
extern Semaphore* tics10thsec;
extern DCnode* deltaclock;
extern TCB tcb[];

Semaphore* mailbox; // doesn't need to be initialized
Semaphore* mailboxMutex; //binary
Semaphore* mailAcquired; //binary
Semaphore* mailboxReady; //binary
Semaphore* needPassenger; //binary
Semaphore* passengerSeated; //binary
Semaphore* seatTaken; //binary
Semaphore* needDriverMutex; //binary
Semaphore* wakeUpDriver; //binary
Semaphore* roomInPark; //counting
Semaphore* roomInMuseum; //counting
Semaphore* roomInGiftShop; //counting
Semaphore* numTicketsAvailable; //counting
Semaphore* needTicket; // binary
Semaphore* takeTicket; // binary
Semaphore* needIceCream; // binary
Semaphore* takeIceCream; // binary
Semaphore* needDriver; // binary
Semaphore* driverMutex; // binary
Semaphore* loadingMutex; // binary
Semaphore* unloadingMutex; // binary
Semaphore* driverTicketResponse; // binary
int carRequestingDriver = -1;

// ***********************************************************************
// project 3 functions and tasks
void CL3_project3(int, char**);
void CL3_dc(int, char**);
int dcMonitorTask(int argc, char* argv[]);
int timeTask(int argc, char* argv[]);

int timeTaskID = -1;

int carTask(int argc, char* argv[]);
int visitorTask(int argc, char* argv[]);
int driverTask(int argc, char* argv[]);

int visitorTask(int argc, char* argv[])
{
	int visitor = atoi(argv[0]);SWAP;
	char buf[32];SWAP;
	sprintf(buf, "visitor%d", visitor);SWAP;
	Semaphore* sem = createSemaphore(buf, COUNTING, 0);SWAP;


	while (1)
	{
		//---------enter park--------------//
		insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // wait to enter the entrance (outside)
		SEM_WAIT(sem);SWAP;

		SEM_WAIT(parkMutex);SWAP;
		myPark.numOutsidePark++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // wait to enter park
		SEM_WAIT(sem);SWAP;

		SEM_WAIT(roomInPark);SWAP;


		//---------ticket line--------------//
		SEM_WAIT(parkMutex);SWAP;
		myPark.numOutsidePark--;SWAP;
		myPark.numInPark++;SWAP;
		myPark.numInTicketLine++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		while (1) // keep trying to get a ticket
		{
			insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // wait in ticket line
			SEM_WAIT(sem);SWAP;

			SEM_WAIT(driverMutex);SWAP;
			SEM_SIGNAL(needTicket);SWAP;
			SEM_SIGNAL(wakeUpDriver);SWAP;
			SEM_WAIT(driverTicketResponse);SWAP;
			int recievedTicket = SEM_TRYLOCK(takeTicket);SWAP;
			SEM_SIGNAL(driverMutex);SWAP;

			if (recievedTicket == 1)
			{
				SEM_WAIT(parkMutex);SWAP;
				myPark.moneyEarned += 1;SWAP;
				SEM_SIGNAL(parkMutex);SWAP;
				break;
			}
		}

		//---------ice cream line-----------//
		SEM_WAIT(parkMutex);SWAP;
		myPark.numInTicketLine--;SWAP;
		myPark.numInIceCreamLine++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // wait in ice cream line
		SEM_WAIT(sem);SWAP;

		SEM_WAIT(driverMutex);SWAP;
		SEM_SIGNAL(needIceCream);SWAP;
		SEM_SIGNAL(wakeUpDriver);SWAP;
		SEM_WAIT(takeIceCream);SWAP;
		SEM_SIGNAL(driverMutex);SWAP;

		SEM_WAIT(parkMutex);SWAP;
		myPark.moneyEarned += 1;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;


		//---------museum line--------------//
		SEM_WAIT(parkMutex);SWAP;
		myPark.numInIceCreamLine--;SWAP;
		myPark.numInMuseumLine++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // wait in museum line
		SEM_WAIT(sem);SWAP;

		SEM_WAIT(roomInMuseum);SWAP;

		SEM_WAIT(parkMutex);SWAP;
		myPark.numInMuseumLine--;SWAP;
		myPark.numInMuseum++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // spend time in museum
		SEM_WAIT(sem);SWAP;


		//----------car line------------//
		SEM_SIGNAL(roomInMuseum);
		SEM_WAIT(parkMutex);SWAP;
		myPark.numInMuseum--;SWAP;
		myPark.numInCarLine++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // wait in car line
		SEM_WAIT(sem);SWAP;

		SEM_WAIT(needPassenger);SWAP; // wait car to need passenger
		SEM_SIGNAL(seatTaken);SWAP; // claim seat

		// pass semaphore to car
		SEM_WAIT(mailboxMutex); SWAP; // wait for mailbox
		mailbox = sem; SWAP; // put semaphore in mailbox
		printf("\nsignaling mailbox");SWAP;
		printf("\nmailbox: %d", sem);SWAP;
		SEM_SIGNAL(mailboxReady); SWAP; // raise the mailbox flag
		SEM_WAIT(mailAcquired); SWAP; // wait for delivery
		SEM_SIGNAL(mailboxMutex); SWAP; // release mailbox

		SEM_WAIT(passengerSeated);SWAP; // wait to be seated

		SEM_SIGNAL(numTicketsAvailable);SWAP; // release ticket
		SEM_WAIT(parkMutex);SWAP;
		myPark.numTicketsAvailable++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		SEM_WAIT(parkMutex);SWAP;
		myPark.numInCarLine--;SWAP;
		myPark.numInCars++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		SEM_WAIT(sem);SWAP; // wait for car to tell me when ride is over
		printf("\ni rode the car!");SWAP;


		//---------gift shop line--------------//
		SEM_WAIT(parkMutex);SWAP;
		myPark.numInCars--;SWAP;
		myPark.numInGiftLine++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // wait in gift shop line
		SEM_WAIT(sem);SWAP;

		SEM_WAIT(roomInGiftShop);SWAP;

		SEM_WAIT(parkMutex);SWAP;
		myPark.numInGiftLine--;SWAP;
		myPark.moneyEarned += rand()%3+1;SWAP;
		myPark.numInGiftShop++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		insertDeltaClock(deltaclock, rand()%(10*10+1), sem);SWAP; // spend time in gift shop
		SEM_WAIT(sem);SWAP;


		//----------exit park-----------//
		SEM_SIGNAL(roomInGiftShop);SWAP;
		SEM_SIGNAL(roomInPark);SWAP;
		SEM_WAIT(parkMutex);SWAP;
		myPark.numInGiftShop--;SWAP;
		myPark.numInPark--;SWAP;
		myPark.numExitedPark++;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;
	}

	return 0;
}

int carTask(int argc, char* argv[])
{
	int car = atoi(argv[0]);SWAP;
	Semaphore* carVisitor[NUM_SEATS];SWAP;
	Semaphore* carDriver;SWAP;

	while (1)
	{
		SEM_WAIT(loadingMutex);SWAP; // wait to load
		for (int i=0; i<NUM_SEATS; i++) // for each seat
		{
			printf("\ncar %d wait fill seat %d", car, i);SWAP;
			SEM_WAIT(fillSeat[car]);SWAP; // wait for available seat

			printf("\nwait seat taken");SWAP;
			SEM_SIGNAL(needPassenger);SWAP; // signal for visitor
			SEM_WAIT(seatTaken);SWAP; // wait for visitor to reply

			printf("\nwait visitor mailbox");SWAP;
			// save passenger ride over semaphore
			SEM_WAIT(mailboxReady);SWAP; // wait for mail
			carVisitor[i] = mailbox;SWAP; // get mail
			SEM_SIGNAL(mailAcquired);SWAP; // put flag down

			SEM_SIGNAL(passengerSeated);SWAP; // signal visitor in seat
			// if last passenger, get driver
			if (i == NUM_SEATS-1)
			{
				printf("\nwait driver");SWAP;
				SEM_WAIT(driverMutex);SWAP; // wait for driver mutex
				carRequestingDriver = car;SWAP;
				printf("\ngot driver mutex");SWAP;
				SEM_SIGNAL(needDriver);SWAP; // wait for driver mutex
				SEM_SIGNAL(wakeUpDriver);SWAP; // wake up driver

				printf("\nwait driver mailbox");SWAP;
				// save driver ride over semaphore
				SEM_WAIT(mailboxReady);SWAP; // wait for mail
				carDriver = mailbox;SWAP; // get mail
				SEM_SIGNAL(mailAcquired);SWAP; // put flag down

				printf("\nsignal got driver");SWAP;
				SEM_SIGNAL(driverMutex);SWAP; // got driver mutex
			}
			
			SEM_SIGNAL(seatFilled[car]);SWAP; // signal next seat ready
		}
		SEM_SIGNAL(loadingMutex);SWAP; // stop loading
		printf("\nwait ride over");SWAP;
		SEM_WAIT(rideOver[car]);SWAP; // wait for ride over
		
		// release passengers and driver
		SEM_WAIT(unloadingMutex);SWAP; // wait to unload
		for (int i=0; i<NUM_CARS; i++)
		{
			SEM_SIGNAL(carVisitor[i]);SWAP; // tell the poor guy the ride is over
		}
		SEM_SIGNAL(carDriver);SWAP; // tell the poor guy the ride is over
		SEM_SIGNAL(unloadingMutex);SWAP; // stop unloading
	}

	return 0;
}

int driverTask(int argc, char* argv[])
{
	char buf[32];SWAP;
	int driver = atoi(argv[0]);SWAP;
	printf(buf, "Starting driverTask%d", driver);SWAP;
	sprintf(buf, "driverDone%d", driver + 1);SWAP;
	Semaphore* driverDone = createSemaphore(buf, BINARY, 0);SWAP;
	Semaphore* display = createSemaphore(buf, BINARY, 0);SWAP;

	int delay = 0;SWAP;
	while (1)
	{
		if (delay > 0)
		{
			delay--;SWAP;
			continue;
		}
		SEM_WAIT(parkMutex);SWAP;
		myPark.drivers[driver] = 0;SWAP;
		SEM_SIGNAL(parkMutex);SWAP;

		SEM_WAIT(wakeUpDriver);SWAP;

		int driverNeeded = SEM_TRYLOCK(needDriver);SWAP;
		if (driverNeeded == 1)
		{
			SEM_WAIT(parkMutex);SWAP;
			myPark.drivers[driver] = carRequestingDriver+1;SWAP;
			SEM_SIGNAL(parkMutex);SWAP;

			mailbox = driverDone;SWAP; // pass notification semaphore
			SEM_SIGNAL(mailboxReady);SWAP; // driver ready
			//SEM_WAIT(carReady);SWAP; // wait for car ready to go
			SEM_WAIT(mailAcquired);SWAP; // make sure the car gets my semaphore
			SEM_WAIT(driverDone);SWAP; // drive ride
			continue;
		}

		int ticketNeeded = SEM_TRYLOCK(needTicket);SWAP
		if (ticketNeeded == 1)
		{
			SEM_WAIT(parkMutex);SWAP;
			myPark.drivers[driver] = -1;SWAP;
			SEM_SIGNAL(parkMutex);SWAP;

			insertDeltaClock(deltaclock, 1, display);SWAP; // wait to enter the entrance (outside)
			SEM_WAIT(display);SWAP;

			printf("\ndriver giving ticket: %d", driver);SWAP;
			delay = 100;SWAP;

			int ticketAvailable = SEM_TRYLOCK(numTicketsAvailable);SWAP; // try to get ticket
			if (ticketAvailable == 1)
			{
				SEM_WAIT(parkMutex);SWAP;
				myPark.numTicketsAvailable--;SWAP;
				SEM_SIGNAL(parkMutex);SWAP;
				SEM_SIGNAL(takeTicket);SWAP; // print a ticket (binary)
			}
			SEM_SIGNAL(driverTicketResponse);SWAP; // respond to ticket request
			continue;
		}

		int iceCreamNeeded = SEM_TRYLOCK(needIceCream);SWAP
		if (iceCreamNeeded == 1)
		{
			SEM_WAIT(parkMutex);SWAP;
			myPark.drivers[driver] = 5;SWAP;
			SEM_SIGNAL(parkMutex);SWAP;

			printf("\ndriver giving ice cream: %d", driver);SWAP;
			delay = 100;SWAP;

			SEM_SIGNAL(takeIceCream);SWAP; // respond to ice cream request
			continue;
		}

		assert(FALSE); // if you got here, there's an error with semtrylock or your park tasks
	}
	return 0;
}

// ***********************************************************************
// ***********************************************************************
// project3 command
int P3_project3(int argc, char* argv[])
{
	char buf[32];
	char* newArgv[2];

	mailboxMutex = createSemaphore("mailboxMutex", BINARY, 1);
	mailAcquired = createSemaphore("mailAcquired", BINARY, 0);
	mailboxReady = createSemaphore("mailboxReady", BINARY, 0);
	needPassenger = createSemaphore("needPassenger", BINARY, 0);
	passengerSeated = createSemaphore("passengerSeated", BINARY, 0);
	seatTaken = createSemaphore("seatTaken", BINARY, 0);
	needDriverMutex = createSemaphore("needDriverMutex", BINARY, 1);
	wakeUpDriver = createSemaphore("wakeUpDriver", BINARY, 0);
	roomInPark = createSemaphore("roomInPark", COUNTING, MAX_IN_PARK);
	roomInMuseum = createSemaphore("roomMuseum", COUNTING, MAX_IN_MUSEUM);
	roomInGiftShop = createSemaphore("roomGiftShop", COUNTING, MAX_IN_GIFTSHOP);
	numTicketsAvailable = createSemaphore("numTicketsAvailable", COUNTING, MAX_TICKETS);
	needTicket = createSemaphore("needTicket", BINARY, 0);
	takeTicket = createSemaphore("takeTicket", BINARY, 0);
	needIceCream = createSemaphore("needIceCream", BINARY, 0);
	takeIceCream = createSemaphore("takeIceCream", BINARY, 0);
	needDriver = createSemaphore("needDriver", BINARY, 0);
	driverMutex = createSemaphore("driverMutex", BINARY, 1);
	loadingMutex = createSemaphore("loadingMutex", BINARY, 1);
	unloadingMutex = createSemaphore("unloadingMutex", BINARY, 1);
	driverTicketResponse = createSemaphore("driverTicketResponse", BINARY, 1);

	// start park
	sprintf(buf, "jurassicPark");
	newArgv[0] = buf;
	createTask( buf,				// task name
		jurassicTask,				// task
		MED_PRIORITY,				// task priority
		1,								// task count
		newArgv);					// task argument

	// wait for park to get initialized...
	while (!parkMutex) SWAP;
	printf("\nStart Jurassic Park...");

	// start delta clock timer
	timeTaskID = createTask( "Time",		// task name
		timeTask,	// task
		HIGH_PRIORITY,			// task priority
		argc,			// task arguments
		argv);

	// make car tasks
	for (int i=0; i<NUM_CARS; i++)
	{
		int argc = 1;
		char** argv = (char**) malloc(argc * sizeof(char*));SWAP;
		argv[0] = (char*) malloc(sizeof(char*));SWAP;
		sprintf(buf, "car%d", i);SWAP;
		sprintf(argv[0], "%d", i);SWAP;
		createTask(buf, carTask, MED_PRIORITY, argc, argv);SWAP;
		free(argv[0]);SWAP;
		free(argv);SWAP;
	}

	// make visitor tasks
	for (int i=0; i<NUM_VISITORS; i++)
	{
		int argc = 1;
		char** argv = (char**) malloc(argc * sizeof(char*));SWAP;
		argv[0] = (char*) malloc(sizeof(char*));SWAP;
		sprintf(buf, "visitor%d", i);SWAP;
		sprintf(argv[0], "%d", i);SWAP;
		createTask(buf, visitorTask, MED_PRIORITY, argc, argv);SWAP;
		free(argv[0]);SWAP;
		free(argv);SWAP;
	}

	// make driver tasks
	for (int i=0; i<NUM_DRIVERS; i++)
	{
		int argc = 1;
		char** argv = (char**) malloc(argc * sizeof(char*));SWAP;
		argv[0] = (char*) malloc(sizeof(char*));SWAP;
		sprintf(buf, "driver%d", i);SWAP;
		sprintf(argv[0], "%d", i);SWAP;
		createTask(buf, driverTask, MED_PRIORITY, argc, argv);SWAP;
		free(argv[0]);SWAP;
		free(argv);SWAP;
	}

	return 0;
} // end project3

// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// ***********************************************************************
// delta clock command
int P3_dc(int argc, char* argv[])
{
	printDeltaClock(deltaclock);
	return 0;
} // end CL3_dc

/*
int P3_tdc(int argc, char* argv[])
{
	testDeltaClock(); // my custom test delta clock function
	return 0;
}
*/

// ***********************************************************************
// test delta clock
int P3_tdc(int argc, char* argv[])
{
	createTask( "DC Test",			// task name
		dcMonitorTask,		// task
		HIGH_PRIORITY,					// task priority
		argc,					// task arguments
		argv);

	timeTaskID = createTask( "Time",		// task name
		timeTask,	// task
		HIGH_PRIORITY,			// task priority
		argc,			// task arguments
		argv);
	return 0;
} // end P3_tdc



// ***********************************************************************
// monitor the delta clock task
int dcMonitorTask(int argc, char* argv[])
{
	assert(deltaclock != NULL);

	int i, flg;
	char buf[32];
	// create some test times for event[0-9]
	int ttime[10] = {90, 300, 50, 170, 340, 300, 50, 300, 40, 110};
	//int ttime[10] = {9, 3, 5, 1, 3, 3, 5, 3, 4, 1};
	Semaphore** event = (Semaphore**)malloc(10*sizeof(Semaphore*));
	for (i=0; i<10; i++)
	{
		sprintf(buf, "event[%d]", i);
		event[i] = createSemaphore(buf, BINARY, 0);
		insertDeltaClock(deltaclock, ttime[i], event[i]);
	}
	printDeltaClock(deltaclock);

	while (deltaclock->time > 0)
	{
		SEM_WAIT(dcChange);
		//printf("\nmonitoring");
		flg = 0;
		for (i=0; i<10; i++)
		{
			if (event[i]->state == 1)
			{
				printf("\n  event[%d] signaled", i);
				event[i]->state = 0;
				flg = 1;
			}
		}
		if (flg) printDeltaClock(deltaclock);
	}
	printf("\nNo more events in Delta Clock");

	// kill dcMonitorTask
	killTask(timeTaskID);
	
	for (i=0; i<10; i++)
		deleteSemaphore(event[i]);
	free(event);
	
	return 0;
} // end dcMonitorTask



// ********************************************************************************************
// display time every tics1sec and tic down the first node's time in deltaclock
int timeTask(int argc, char* argv[])
{
	char svtime[64];						// ascii current time
	while (1)
	{
		SEM_WAIT(dcAccess);
		while(deltaclock->next != NULL && deltaclock->next->time <= 0)
		{
			if (deltaclock->next->sem != NULL)
			{
				printf("\nsemsignal: %s", deltaclock->next->sem->name);
				SEM_SIGNAL(deltaclock->next->sem);
			}
			else
				printf("\nnull sem");
			DCnode* prev = deltaclock->next;
			deltaclock->next = deltaclock->next->next;
			free(prev);
			deltaclock->time--; // decrement node count
			SEM_SIGNAL(dcChange);
		}
		SEM_SIGNAL(dcAccess);
		//printf("\nTime = %s", myTime(svtime));
		SEM_WAIT(tics10thsec)
		SEM_WAIT(dcAccess);
		if (deltaclock->next != NULL)
			deltaclock->next->time--;
		SEM_SIGNAL(dcAccess);
	}
	return 0;
} // end timeTask
